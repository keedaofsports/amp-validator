var amphtmlValidator = require('amphtml-validator');
var request = require("request");

exports.handler = (event, context, callback) => {

    var ampURL = event.url;
    request({ uri: ampURL },
      function(error, response, body) {
          amphtmlValidator.getInstance().then(function(validator) {
            var result = validator.validateString(body);
            (result.status === 'PASS' ? console.log : console.error)(result.status);
            for (var ii = 0; ii < result.errors.length; ii++) {
              var error = result.errors[ii];
              var msg =
                'line ' + error.line + ', col ' + error.col + ': ' + error.message;
              if (error.specUrl !== null) {
                msg += ' (see ' + error.specUrl + ')';
              }
              (error.severity === 'ERROR' ? console.error : console.warn)(msg);
            }
          });
      });

  console.log("URL is " + event.url);
}