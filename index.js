var http = require('http');
var urlLib = require('url');
var port = process.env.NODE_PORT || 8989;

//create a server object:
http.createServer(function (req, res) {
    res.writeHead(200, {'Content-Type': 'application/json'}); // http header
    var url = req.url;
    var urlData = urlLib.parse(url, true);
    console.log(urlData.query)
    if(url.indexOf("/validate/") >= 0 && url.split('/').length >= 3) {
        var amphtmlValidator = require('amphtml-validator');
        var request = require("request");
        request({ uri: urlData.query.url },
            function(error, response, body) {
                if(!!error) {
                    res.write(error.message);
                    res.end();
                }
                amphtmlValidator.getInstance().then(function(validator) {
                    var result = validator.validateString(body);
                    (result.status === 'PASS' ? console.log : console.error)(result.status);
                    if (result.status === 'PASS') {
                      var resultObj = {"valid" : true};
                    } else {
                      var resultObj = {"valid" : false, "errors" : result.errors};
                    }
                    res.write(JSON.stringify(resultObj));
                    res.end();
                });
            },
            );
    } else {
        res.write("Invalid request");
        res.end();
    }
}).listen(port, function(){
    console.log("[INFO] server start at port",port); //the server object listens on port 3000
});
