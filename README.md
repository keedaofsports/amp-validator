# AMP Validator Service

The service is to perform validation of amp pages. 

## Installation & Setup

* Clone repo
* Install project dependencies: ```npm install```


## Usage
* To run the application: ```npm start```
* Service will run on port 8989
* To validate an amp page, Go to ```http://localhost:8989/validate/?url=<URL to validate>``` On browser, 

## Deployment
Please check the Deployment section of the spec from [here](https://docs.google.com/document/d/1JtACmvqjKanxZURFpRdysRBufCXa1ecGZFFzPslx3RQ/edit#heading=h.x456kxulu9n8)
